#!/bin/bash

#-----------------------------------------
#           upgrade2centos7Part2.sh
#
# Description :Script pour migrer une CentOS6 vers CentOS7
# Auteur : Ludovic Pastor (LIG-MI)
# Date 19/02/2021
# Version : 1.6
#-----------------------------------------


#-----------------------------------------
#           FUNCTIONS
#-----------------------------------------

#Fonction pour la post-upgrade
function after_reboot(){
    
    #-----------------------------------------
    #Timestamp for logging
    DATE=$(date --rfc-3339=seconds)
    LOG_FILE=/opt/centos6to7.log
    exec &> >(tee -a ${LOG_FILE})
    echo "##### Second part of the migration executed in $DATE #####"
    
    echo "### Fixing duplicated packages... ###"
    DuplicatePackages
    echo "### Fixing duplicated packages completed ###"
    
    echo "### Managing repos in CentOS-Base.repo... ###"
    Managing_repos
    echo "### Repos management completed ###"
    
    echo "### Fixing rpm packages... ###"
    fix_rpms

    # Completed update for CentOS7.2.1511
    echo "### Completing update for CentOS7.2 ###"
    yum clean all
    yum makecache fast
    yum update -y
    
    echo "##### Upgrade to 7.8.2003 #####"
    centos72To78
    
    echo "##### Upgrade to the last release #####"
    Centos78ToLastRelease
    
    echo "### Managing services in SystemD ###"
    services

    echo "Configuration of grub2"
    yum install -y grub2
    #Création du fichier de config de grub2
    #Se base sur les fichier de conf dans /boot
    grub2-mkconfig -o /boot/grub2/grub.cfg
    #installation du grub2 sur la partition boot
    grub2-install /dev/sda

}

#####################################
#       POST-UPGRADE FUNCTIONS      #
#####################################

# Fonction pour les paquets dupliques en el6 et el7
function DuplicatePackages(){

    rpm -e python-argparse-1.2.1-2.1.el6.noarch
    rpm -e python2-distro-1.0.3-1.el6.noarch
    rpm -e openscap-1.2.13-2.el6
    rpm -e redhat-upgrade-tool-0.7.22-3.el6.centos.noarch

}

# Fonction pour supprimer les depots Red Hat et Upgrade utilise pour la mise a niveau
function Managing_repos(){
    
    rm -Rf /etc/yum.repos.d/redhat-upgrade-*
    rm -Rf /etc/yum.repos.d/upgrade.repo
    mv /etc/yum.repos.d/CentOS-Base.repo.rpmnew /etc/yum.repos.d/CentOS-Base.repo
    # Commenter la ligne mirrorlist dans Centos-Base
    sed -i "s\mirrorlist=http://mirrorlist.centos.org/?release=${releasever:?}\#mirrorlist=http://mirrorlist.centos.org/?release=$releasever\g" /etc/yum.repos.d/CentOS-Base.repo

    # Decommenter la ligne baseurl et passer sur vault.centos.org pour centos 7.2.1511
    #sed -i 's\#baseurl=http://mirror.centos.org/centos/${releasever}/\baseurl=https://vault.centos.org/7.2.1511/\g' /etc/yum.repos.d/CentOS-Base.repo
    sed -i "s\#baseurl=http://mirror.centos.org/centos/${releasever}/\baseurl=http://centos-repo.imag.fr/7.2.1511/\g" /etc/yum.repos.d/CentOS-Base.repo

}

# Fonction pour corriger les problemes avec les paquets el6 restants
function fix_rpms(){
    mkdir -p /opt/centos_7
    
    # for loop to compare el6 packages still in the system and download el7 packages equivalent from LIG repo
    package_list=$(rpm -qa --qf "%{NAME}-%{RELEASE}\n" | grep ".el6" | cut -d '.' -f 1 | sed 's/-[0-9]\+//g')

    echo "### Downloading required packages... ###"
    for package in $package_list
    do
        wget -4 -P /opt/centos_7/ -r -nd -l1 --no-parent --no-check-certificate  -A "$package*.rpm" http://centos-repo.imag.fr/centos_7/
    done
    
    #Deleting html files
    rm -rf /opt/centos_7/index.html*

    echo "### Replacing el6 packages... ###"
    el6_packages=$(rpm -qa --qf "%{NAME}-%{RELEASE}\n" | grep ".el6" | cut -d '.' -f 1)
    for package in $el6_packages
    do
        if rpm -Uvh /opt/centos_7/"$package"* --nodeps --replacefiles --replacepkgs --oldpackage
         then
            echo "version of $package too different..."
            package=$(echo "$package" | sed 's/-[0-9]\+//g')
            echo "$package"
            rpm -Uvh /opt/centos_7/"$package"-* --nodeps --replacefiles --replacepkgs --oldpackage
        fi
    done

    #Checking centos 6 package left
    echo "### Cleaning lefted el6 packages ###"
    rpm -e python-libs-2.6.6-68.el6_10.i686
    rpm -e glibc-2.17-105.el7.i686 --nodeps
    
    el6_rest=$(rpm -qa | grep ".el6")
    
    for package in $el6_rest
    do
        echo "The package $package will be deleted"
        rpm -e "$package" --nodeps
    done
    
}

function centos72To78(){
    #Changing baseurl line from 7.2.1511 to 7.8.2003
    sed -i 's\baseurl=http://centos-repo.imag.fr/7.2.1511/\baseurl=http://centos-repo.imag.fr/7.8.2003/\g' /etc/yum.repos.d/CentOS-Base.repo
    yum clean all
    yum update -y
}

function Centos78ToLastRelease(){
    
    # Uncommented mirrorlist line in centos-base.repo
    sed -i "s\#mirrorlist=http://mirrorlist.centos.org/?release=${releasever}\mirrorlist=http://mirrorlist.centos.org/?release=$releasever\g" /etc/yum.repos.d/CentOS-Base.repo
    #commented baseurl line in centos-base.repo
    sed -i 's\baseurl=http://centos-repo.imag.fr/7.8.2003/\#http://centos-repo.imag.fr/7.8.2003/\g' /etc/yum.repos.d/CentOS-Base.repo

    rm -Rf /var/cache/yum/x86_64/6/
    rpm -e nspr-devel-4.21.0-1.el7.x86_64 --nodeps
    rpm -Uvh postrpm/nspr-4.25.0-2.el7_9.x86_64.rpm

    # Update to last release of CentOS
    yum clean all
    yum makecache fast
    yum update -y

}

# Managing SysVinit service to SystemD
function services(){
    ####VARIABLES#####

    #Adding sshd ip6tables and puppet to services to start
    echo -e "sshd\nip6tables\nnetwork\npuppet" >> /opt/services
    
    servicesCentos6=/opt/services
    systemctl list-unit-files --type=service --state=disabled | grep \.service| awk '{print $1}' |cut -d '.' -f 1 > /opt/servicesSystemD
    servicesSystemD=/opt/servicesSystemD
    grep -wFf $servicesCentos6 $servicesSystemD > /opt/servicesInSysD
    servicesExist=$(cat /opt/servicesInSysD)
    
    #For loop in order to set in enable mode services
    for service in $servicesExist
    do
        echo "The service $service will be started and enabled..."
        if systemctl start "$service" && systemctl enable "$service"
          then
            echo "error for $service in systemD : check logs"
        fi

    done
    
}
#-----------------------------------------
#           MAIN
#-----------------------------------------
#### Check root privileges ####
if [ $EUID -ne 0 ]; then
    echo "You need root privileges to execute this script"
    exit 1
fi

after_reboot

reboot