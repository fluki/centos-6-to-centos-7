#!/bin/bash

#-----------------------------------------
#           upgrade2centos7Part1.sh
#
# Description :Script pour migrer une CentOS6 vers CentOS7
# Auteur : Ludovic Pastor (LIG-MI)
# Date 19/02/2021
# Version : 1.6
#-----------------------------------------

#-----------------------------------------
#           FUNCTIONS
#-----------------------------------------

# Upgrade function
function before_reboot(){

    #-----------------------------------------
    #Timestamp for logging
    DATE=$(date --rfc-3339=seconds)
    LOG_FILE=/opt/centos6to7.log
    exec &> >(tee -a ${LOG_FILE})
    echo "##### First part of the migration executed in $DATE #####"
    
    pre_requis
    
    DE_check
    
    #-----------------------------------------

    # Desabling puppet service for upgrade
    
    if service --status-all | grep running | grep puppet
    then
        service puppet stop
        chkconfig puppet off
    fi

    #-----------------------------------------
    # UPDATE REPO
    #Adding repo for redhat-upgrade-tool 
    echo -e "[centos-upgrade]\nname=centos-upgrade\nbaseurl=http://centos-repo.imag.fr/upgrade/6/upg/x86_64/\nenabled=1\ngpgcheck=0" > /etc/yum.repos.d/upgrade.repo

    # INSTALL OPENSCAP
    yum -y install http://centos-repo.imag.fr/upgrade/6/upg/x86_64/Packages/openscap-1.0.8-1.0.1.el6.centos.x86_64.rpm

    # INSTALL UPGRADE TOOLS
    yum -y install preupgrade-assistant-contents redhat-upgrade-tool preupgrade-assistant
    
    echo "y" | preupg

    # ADD RPM GPG KEY FOR CENTOS 7
    rpm --import http://mirror.centos.org/centos/RPM-GPG-KEY-CentOS-7
    
    #-----------------------------------------
    
    #Avoid checksum errors for centos-upgrade-tool
    mkdir -pv /var/tmp/system-upgrade/{base,extras,updates}/

    echo http://mirror.centos.org/centos/7/os/x86_64/ >> /var/tmp/system-upgrade/base/mirrorlist.txt
    echo http://mirror.centos.org/centos/7/extras/x86_64/ >> /var/tmp/system-upgrade/extras/mirrorlist.txt
    echo http://mirror.centos.org/centos/7/updates/x86_64/ >> /var/tmp/system-upgrade/updates/mirrorlist.txt

    #-----------------------------------------

    #Modifying z_copy_clean_conf.sh file to fix grep issues, systemd and rpm, yum for post-upgrade
    echo -e "## Fix grep ##\nrpm -Uvh /opt/postrpm/grep-2.20-6.el7.x86_64.rpm --nodeps --replacefiles --replacepkgs --oldpackage --force\nrpm -Uvh /opt/postrpm/nspr-4.21.0-1.el7.x86_64.rpm --nodeps --replacefiles --replacepkgs --oldpackage --force\nrpm -Uvh /opt/postrpm/nspr-devel-4.21.0-1.el7.x86_64.rpm --nodeps --replacefiles --replacepkgs --oldpackage --force\nrpm -Uvh /opt/postrpm/nss-3.19.1-18.el7.x86_64.rpm --nodeps --replacefiles --replacepkgs --oldpackage --force\nrpm -Uvh /opt/postrpm/nss-pam-ldapd-0.8.13-8.el7.x86_64.rpm --nodeps --replacefiles --replacepkgs --oldpackage --force\nrpm -Uvh /opt/postrpm/nss-softokn-3.16.2.3-13.el7_1.x86_64.rpm --nodeps --replacefiles --replacepkgs --oldpackage --force\nrpm -Uvh /opt/postrpm/nss-softokn-freebl-3.16.2.3-13.el7_1.x86_64.rpm --nodeps --replacefiles --replacepkgs --oldpackage --force\nrpm -Uvh /opt/postrpm/nss-sysinit-3.19.1-18.el7.x86_64.rpm --nodeps --replacefiles --replacepkgs --oldpackage --force\nrpm -Uvh /opt/postrpm/nss-tools-3.19.1-18.el7.x86_64.rpm --nodeps --replacefiles --replacepkgs --oldpackage --force\nrpm -Uvh /opt/postrpm/nss-util-3.19.1-4.el7_1.x86_64.rpm --nodeps --replacefiles --replacepkgs --oldpackage --force\n## Fix libsasl2 ##\nln -s /lib64/libsasl2.so.3 /lib64/libsasl2.so.2\n## Essentials services ##\nsystemctl start sshd && systemctl enable sshd\nsystemctl start nslcd && systemctl enable nslcd\nsystemctl start rc-local && systemctl enable rc-local" > /opt/z_clean

    sed -i '/#!/r z_clean' /root/preupgrade/postupgrade.d/z_copy_clean_conf.sh

    #-----------------------------------------
    # UPGRADE COMMAND TO CENTOS 7
    centos-upgrade-tool-cli -f --network=7.2 --instrepo=http://centos-repo.imag.fr/7.2/os/x86_64/ --cleanup-post --nogpgcheck  -d
    
}
#########################################
#       BEFORE UPGRADE FUNCTIONS        #
#########################################

# prerequisite checks
function pre_requis(){

    #Downloading packages from http:/centos-repo.imag.fr/
    wget -4 -P /opt/ -nH -rkpN -e robots=off --no-parent http://centos-repo.imag.fr/postrpm/
    wget -4 -P /opt/postrpm/ -r -nd -l1 --no-parent --no-check-certificate  -A "nss*.rpm" http://centos-repo.imag.fr/centos_7/
    #Installing grep package
    rpm -Uvh --oldpackage /opt/postrpm/grep-2.20-2.el6.x86_64.rpm 
    
    echo -e "## Exclude following Packages Updates ##\nexclude=grep nspr*" >> /etc/yum.conf
    yum install -y recode
    
    #Removing EPEL repo
    yum remove epel-release -y
    #cache creation for repos
    yum makecache fast
    #utility packages for yum
    yum install -y yum-utils

    #Gathering Centos 6 services in "/opt/services" file
    service --status-all |grep running |awk '{print $1}' > /opt/services

    #Checking SELinux policie
    if grep -q ^SELINUx=disabled /etc/selinux/config; then
        echo "OK"
    else
        #Desabled SELinux by default
        sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config
    fi
    semodule -r sandbox

    #Deleting packages and repos from puppetlabs
    PUPPETLABS=$(yum list | grep puppetlabs|awk '{print $1}')
    for package in $PUPPETLABS
    do
        yum remove -y "$package"
    done

    PUPPETLABS2=$(yum list installed | grep rpmforge | awk '{ print $1 }')
    for package in $PUPPETLABS2
    do
        rpm -e --nodeps "$package"
    done

}

#-----------------------------------------
# Checking Desktop Environnement presence
function DE_check(){
    if pgrep -i "xfce|kde|gnome" > /dev/null
    then
        echo "Desktop environnement exist"
        yum -y groupremove "Desktop" "KDE Desktop" "Graphic*" "Office Suite and Productivity"
    fi
}

#-----------------------------------------
#           MAIN
#-----------------------------------------
#### Check root privileges ####
if [ $EUID -ne 0 ]; then
    echo "You need root privileges to execute this script"
    exit 1
fi

before_reboot

reboot